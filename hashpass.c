#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    // hashpass may or may not be a slightly modified version of our copy lab ¯\_(ツ)_/¯


    // check number of arguments
    // argc == # of arguments
    // argv == array of values
    if (argc == 1)
    {
        printf("You need to enter a file name... baka (`へ´*)ノ\n");
        exit(1);
    }
    
    if (argc == 2)
    {
        printf("You need to enter 2 file names... aho (`へ´*)ノ\n");
        exit(1);
    }
    
    // check to see if input & output are a directories
    // for loop goes through both args, a is a flag
    for (int i = 1; i < 3; i ++)
    {
        int dir = 0;
        struct stat filestat;
        stat(argv[1], &filestat);
        if ((filestat.st_mode & S_IFMT) == S_IFDIR)
        {
            printf("%s is a directory\n", argv[1]);
            dir = 1;
        }
        
        if (dir)
            exit(1);
    }
    
    // check if input can be opened    
    FILE *inptr;
    inptr = fopen(argv[1], "r");
    if (inptr == NULL)
    {
        printf("Cant't open %s for reading\n", argv[1]);
        exit(1);
    }

    // check if output can be made 
    FILE* outptr = fopen(argv[2], "w");
    if (outptr == NULL)
    {
        fclose(inptr);
        fprintf(stderr, "Could not create %s.\n", argv[2]);
    }
    
    // read & hash lines from inptr
    // write the hash into outptr
    // rinse & repeat until eof
    char line[16];
    while (fgets(line, 16, inptr))
    {
        // take off newline char
        int len = strlen(line);
        if (len > 0 && line[len-1] == '\n')
        {
            line[len-1] = '\0';
            printf("%s\n", line);
        }
            
        // hash the string
        char *hash = md5(line, len-1);
        
        // write hash into outputr
        // free hash because of the malloc
        fprintf(outptr, "%s\n", hash);
        free(hash);
    }
 
    printf("Hashes written to %s\n", argv[2]);
    
    // close infile
    fclose(inptr);

    // close outfile
    fclose(outptr);
}